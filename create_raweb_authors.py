import configparser
from pymongo import MongoClient
from elasticsearch import Elasticsearch

def replace_special_caract(text) :
    text = text.replace("é", "e")
    text = text.replace("è", "e")
    text = text.replace("ê", "e")
    text = text.replace("ë", "e")
    text = text.replace("à", "a")
    text = text.replace("ï", "i")
    text = text.replace("î", "i")
    text = text.replace("ô", "o")
    text = text.replace("ç", "c")
    text = text.replace("ü", "u")
    text = text.replace("ù", "u")
    text = text.replace("-", " ")
    return text

#Create author
def create_author(member, value, key, structure) :
    dict_author = {}
    dict_author["surname"] = member["lastname"]
    dict_author["forename"] = member["firstname"]
    dict_author["acronym"] = key.upper()
    dict_author["structure"] = structure
    dict_author["formatName"] = replace_special_caract(member["firstname"].lower().strip()+" "+member["lastname"].lower().strip())
    dict_author["first_appear"] = value["year"]
    dict_author["last_appear"] = value["year"]
    dict_author["moreinfos"] = member["moreinfos"]
    dict_author["category"] = member["category"]
    return dict_author

#update data author
def update_author(member, value, array_member_dict) :
    dict_author = {}
    i = 0
    for dict_authors in array_member_dict :
        dict_author = dict_authors
        if dict_author["formatName"] == replace_special_caract(member["firstname"].lower().strip()+" "+member["lastname"].lower().strip()) :
            if dict_author["first_appear"] > value["year"] :
                dict_author["first_appear"] = value["year"]
            if dict_author["last_appear"] < value["year"] :
                dict_author["last_appear"] = value["year"]
                if dict_author["category"] != member["category"] and member["category"] != "" :
                    dict_author["category"] = member["category"]
                if dict_author["moreinfos"] != member["moreinfos"] and member["moreinfos"] != "":
                    dict_author["moreinfos"] = member["moreinfos"]
            array_member_dict[i] = dict_author
            break
        i = i+1
    return array_member_dict


# Config properties
config = configparser.RawConfigParser()
config.read("ConfigFile.properties")

mongodb_ip = config.get("mongodb", "ip")
mongodb_port = int(config.get("mongodb", "port"))
mongodb_db = config.get("mongodb", "database")

client = MongoClient(mongodb_ip, mongodb_port)

db = client.get_database(mongodb_db)

posts = db.posts

collection = db.get_collection('teams')
data = collection.find()

clean_team_list = {"magrite", "miro", "métalau", "virtual_plants", "virtualplants", "eptar", "congé", "euréca", "opéra", "langue_et_dialogue",
                    "psychoergo", "pop_art", "opéra/rhône-alpes", "popart", "sémagramme", "résédas", "méta2", "méval", "sigma2", "sigma 2", "méta 2",
                    "ep-atr", "epatr", "ep ­atr" }

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_team = config.get("elasticsearch", "index_team")
index_raweb_author = config.get("elasticsearch", "index_raweb_author")

dict_team = {}
for d in data :
    array_year = []
    if d["acronym"] in clean_team_list :
        if "é" in d["acronym"] :
            d["acronym"] = d["acronym"].replace("é", "e")
        if "_" in d["acronym"] :
            d["acronym"] = d["acronym"].replace("_", " ")
        if "/" in d["acronym"] :
            d["acronym"] = d["acronym"].split("/")[0]
        if d["acronym"] == "magrite" :
            d["acronym"] = "magrit"
        if d["acronym"] == "miro" :
            d["acronym"] = "mirho"
        if d["acronym"] == "virtualplants" :
            d["acronym"] = "virtual plants"
        if d["acronym"] == "popart" :
            d["acronym"] = "pop art"
        if d["acronym"] == "meta 2" :
            d["acronym"] = "meta2"
        if d["acronym"] == "sigma 2" :
            d["acronym"] = "sigma2"
    if d["acronym"] == "psycho­ergo" or d["acronym"] == "psycho-ergo" :
        d["acronym"] = "psycho­ ergo"
    if d["acronym"] == "vasy-ra" :
        d["acronym"] = "vasy"
    if d["acronym"] == "movi-grenoble" :
        d["acronym"] = "movi"
    if d["acronym"] == "ep-atr" or d["acronym"] == "epatr" or d["acronym"] == "eptar" or d["acronym"] == "ep ­atr" or d["acronym"] == "ep­atr":
        d["acronym"] = "epatr"
    if d["acronym"] == "langue et\n      dialogue" :
        d["acronym"] == "langue et dialogue"
    if d["acronym"] in dict_team :
        array_year = dict_team[d["acronym"]]
        array_year.append(d)
        dict_team[d["acronym"]] = array_year
    else :
        array_year.append(d)
        dict_team[d["acronym"]] = array_year

# Get team years by years
for key, values in dict_team.items() :
    new_team = {}
    new_team["acronym"] = key
    new_team["categories"] = []
    array_member_all = []
    dict_categorie =  []
    i = 0
    query = {"query" : {"match" : {"acronym" : key.upper()}}}
    structure = es.search(index=index_team, body=query)
    if structure["hits"]["total"]["value"] == 0 :
        structure = "NotInHAL"
    else :
        structure = structure["hits"]["hits"][0]["_id"]
    for value in values :
        #Create new team
        array_member_year = {}
        dict_author = {}
        array_member_dict = []
        if i == 0 :
            for member in value["members"] :
                if member["category"] == "" :
                    member["category"] = "unknow"
                dict_author = create_author(member, value, key, structure)
                array_member_all.append(replace_special_caract(member["firstname"].lower().strip()) + " " + replace_special_caract(member["lastname"].lower().strip()))
                dict_categorie.append(dict_author)
        else :
            for member in value["members"] :
                if member["category"] == "" :
                    member["category"] = "unknow"
                if replace_special_caract(member["firstname"].lower().strip()) + " " + replace_special_caract(member["lastname"].lower().strip()) in array_member_all :
                    dict_categorie = update_author(member, value, dict_categorie)
                else :
                    dict_author = create_author(member, value, key, structure)
                    array_member_all.append(replace_special_caract(member["firstname"].lower().strip()) + " " + replace_special_caract(member["lastname"].lower().strip()))

                    dict_categorie.append(dict_author)
        i = i +1
    for authors in dict_categorie :
        es.index(index=index_raweb_author, body=authors)
