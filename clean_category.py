from elasticsearch import Elasticsearch
import configparser

def get_author(search_after, es) :
    query = {
        "size" : 1000,
        "query" : {"match_all" : {}}
    }
    return es.search(index=index_author, body=query,scroll="20m")

def nb_author() :
    return es.count(index=index_author, body={"query" : {"match_all" : {}}})["count"]

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_author = config.get("elasticsearch", "index_author")

research_scientist = ["chercheur", "Chercheur", "Chercheurs invités", "chercheurs invités","Chercheurs associés", "chercheurs associés","Chercheur en mise à disposition"]
phd = ["phd", "PhD", "Chercheurs Doctorants",  "chercheurs Doctorants"]
engineer = ["Technique", "Ingénieur expert", "Assistante de projet", "technique"]
postDoc = ["PostDoc", "Post-doctorants", "post-doctorants", "Chercheurs post-doctorants", "postdoc"]
visiting = ["Visiteur", "Collaborateurs extérieurs", "Professeurs invités", "professeurs invités", "collaborateurexterieur", "visiteur"]
assitant = ["Assistant", "Secrétaire", "secrétaire","assistant"]
autre = ["Autres", "autres"]
faculty_member = ["Enseignant", "Personnel Orléans","Personnel Inria Personnel Ura 227", "Personnel des établissement partenaires", "enseignant", "personnel des établissements partenaires (umr verimag)", "personnel université"]
head = ["Responsable scientifique", "Responsable permanent", "responsable scientifique", "responsable permanent"]
trainee =["Stagiaire", "Stagiaires", "stagiaire", "stagiaires"]

total_raweb_author = nb_author()
range_author = total_raweb_author%1000 + 1

for i in range(0, range_author) :
    if i == 0 :
        authors = get_author(i, es)
    else :
        authors = es.scroll(scroll_id=scroll_id, scroll="20m")
    scroll_id = authors["_scroll_id"]
    for author in authors["hits"]["hits"] :
        change_cat = False
        if "category" in author["_source"] :
            if author["_source"]["category"] in research_scientist :
                author["_source"]["category"] = "Research Scientist"
                change_cat = True
            if author["_source"]["category"] in engineer :
                author["_source"]["category"] = "Engineer"
                change_cat = True
            if author["_source"]["category"] in phd :
                author["_source"]["category"] = "PhD student"
                change_cat = True
            if author["_source"]["category"] in postDoc :
                author["_source"]["category"] = "Postdoct"
                change_cat = True
            if author["_source"]["category"] in visiting :
                author["_source"]["category"] = "Visiting Scientist"
                change_cat = True
            if author["_source"]["category"] in assitant :
                author["_source"]["category"] = "Administrative Assistant"
                change_cat = True
            if author["_source"]["category"] in autre :
                author["_source"]["category"] = "Other"
                change_cat = True
            if author["_source"]["category"] in faculty_member :
                author["_source"]["category"] = "Faculty Member"
                change_cat = True
            if author["_source"]["category"] in head :
                author["_source"]["category"] = "Head"
                change_cat = True
            if author["_source"]["category"] in trainee :
                author["_source"]["category"] = "Trainee"
                change_cat = True
        if change_cat :
            es.update(index=index_author, id=author["_id"], body={"doc" : {"category" : author["_source"]["category"]}})
