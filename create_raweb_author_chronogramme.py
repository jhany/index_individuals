from elasticsearch import Elasticsearch
import configparser

def get_author() :
    query = {
        "size" : 1000,
        "query" : {"match_all" : {}}
    }
    return es.search(index=index_raweb_author, body=query, scroll="30m")

def get_all_pub(forename, surname) :
    query = {
    "size" : 10000,
    "query": {
        "nested": {
           "path": "authors",
           "query": {
               "bool": {
                   "must": [
                      {"match": {
                         "authors.forename": forename
                      }},
                      {
                          "match": {
                             "authors.surname": surname
                          }
                      },
                      {
                          "match": {
                             "authors.mail_domain": "inria.fr"
                          }
                      }
                   ]
               }
           }
        }
    }
    }
    return es.search(index=index_pub, body=query)

def get_all_pub_formatName(formatName) :
    query = {
    "size" : 10000,
                "query" : {
                    "bool": {
                        "must": [
                           {"nested": {
                           "path": "authors",
                           "query": { "match": {
                            "authors.formatName" : formatName
                        }}}},
                        {"nested": {
                           "path": "affiliations.relations.indirect",
                           "query": {
                               "match": {
                                  "affiliations.relations.indirect.acronym": "INRIA"
                               }                                   }
                    }}
                ]
            }
        }
    }
    return es.search(index=index_pub, body=query)


def replace_special_caract(text) :
    text = text.replace("é", "e")
    text = text.replace("è", "e")
    text = text.replace("ê", "e")
    text = text.replace("ë", "e")
    text = text.replace("à", "a")
    text = text.replace("ï", "i")
    text = text.replace("î", "i")
    text = text.replace("ô", "o")
    text = text.replace("ç", "c")
    text = text.replace("ü", "u")
    text = text.replace("ù", "u")
    text = text.replace("-", " ")
    return text

def nb_raweb_author() :
    return es.count(index=index_raweb_author, body={"query" : {"match_all" : {}}})["count"]

config = configparser.RawConfigParser()
config.read("ConfigFile.properties")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_author = config.get("elasticsearch", "index_author")
index_raweb_author = config.get("elasticsearch", "index_raweb_author")
# index_all_pub = config.get("elasticsearch", "index_all_pub")
index_pub = config.get("elasticsearch", "index_pub")

total_raweb_author = nb_raweb_author()
range_author = int(total_raweb_author%1000) + 1

author_allreadydone = []
for i in range(0, range_author) :
    if i == 0 :
        authors = get_author()
    else :
        authors = es.scroll(scroll_id=scroll_id, scroll="30m")
    scroll_id = authors["_scroll_id"]
    for author in authors["hits"]["hits"] :
        author = author["_source"]
        forename = replace_special_caract(author["forename"].lower().strip())
        surname = replace_special_caract(author["surname"].lower().strip())
        query_name = { "query" : {"match" : {"formatName" : forename+" "+surname}}}
        if es.search(index=index_author, body=query_name)["hits"]["total"]["value"] == 0 :
            query_name = { "query" : {"match" : {"formatName.keyword" : forename+" "+surname}}}
            authors_with_key = es.search(index=index_raweb_author, body=query_name)["hits"]["hits"]
            j = 0
            new_author = {}
            new_author["history"] = []
            new_author["pubs"] = []
            year_appear = 0
            #create new author chronogramme
            for author_with_key in authors_with_key :
                author_with_key = author_with_key["_source"]
                history = {}
                if j == 0 :
                    new_author["surname"] = author_with_key["surname"]
                    new_author["forename"] = author_with_key["forename"]
                    new_author["first_appear"] = author_with_key["first_appear"]
                    new_author["last_appear"] = author_with_key["last_appear"]
                    new_author["category"] = author_with_key["category"]
                    new_author["structure"] = author_with_key["structure"]
                    new_author["acronym"] = author_with_key["acronym"].upper()
                    new_author["moreinfos"] = author_with_key["moreinfos"]
                    new_author["formatName"] = replace_special_caract(author["forename"].lower()).strip()+" "+replace_special_caract(author["surname"].lower()).strip()
                    all_publications = get_all_pub_formatName(new_author["formatName"])
                    # if all_publications["hits"]["total"] == 0 :
                    #     all_publications = get_all_pub(new_author["forename"], new_author["surname"])
                    for pub in all_publications["hits"]["hits"] :
                        del pub["_source"]["affiliations"]
                        del pub["_source"]["projects"]
                        new_author["pubs"].append(pub["_source"])
                    year_appear = author_with_key["last_appear"]
                    history["first_appear"] = author_with_key["first_appear"]
                    history["last_appear"] = author_with_key["last_appear"]
                    history["category"] = author_with_key["category"]
                    history["structure"] = author_with_key["structure"]
                    history["acronym"] = author_with_key["acronym"].upper()
                    history["moreinfos"] = author_with_key["moreinfos"]
                    new_author["history"].append(history)
                else :
                    history["first_appear"] = author_with_key["first_appear"]
                    history["last_appear"] = author_with_key["last_appear"]
                    history["category"] = author_with_key["category"]
                    history["structure"] = author_with_key["structure"]
                    history["acronym"] = author_with_key["acronym"].upper()
                    history["moreinfos"] = author_with_key["moreinfos"]
                    new_author["history"].append(history)
                    if year_appear < author_with_key["last_appear"] :
                        year_appear = author_with_key["last_appear"]
                        new_author["first_appear"] = author_with_key["first_appear"]
                        new_author["last_appear"] = author_with_key["last_appear"]
                        new_author["category"] = author_with_key["category"]
                        new_author["structure"] = author_with_key["structure"]
                        new_author["acronym"] = author_with_key["acronym"].upper()
                        new_author["moreinfos"] = author_with_key["moreinfos"]
                j = j +1
            if j != 0 :
                es.index(index=index_author, body=new_author)
